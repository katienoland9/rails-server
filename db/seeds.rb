# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Item.delete_all
Todo.delete_all
5.times do |i|
  todo = Todo.create(
    title:"#{Faker::Hacker.verb}",
    created_by: "#{i}",
    description: "#{Faker::Hacker.say_something_smart}"
  )

    p "\n\nCreated todo: #{todo.title}\n"
    5.times do |j|
      item = Item.create(
        name: "#{Faker::Hacker.say_something_smart}",
        done: Faker::Boolean.boolean,
        todo_id: todo.id
      )

        p "Created Item: #{item.name}"
    end
end
